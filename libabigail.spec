Name:                libabigail
Version:             2.6
Release:             1
Summary:             ABI generic analysis and instrumentation library
License:             LGPL-3.0-or-later
URL:                 https://sourceware.org/libabigail/
Source0:             https://mirrors.kernel.org/sourceware/libabigail/libabigail-%{version}.tar.xz
BuildRequires:       gcc-c++ libtool elfutils-devel doxygen
BuildRequires:       python3-sphinx texinfo dpkg python3-devel python3-rpm
BuildRequires:       python3-mock python3-unittest2 python3-pyxdg wget mailcap
BuildRequires:       pkgconfig(libxml-2.0) >= 2.6.22
BuildRequires:       pkgconfig(libxxhash) >= 0.8.0

%description
The libabigail aims at providing a C++ library for constructing, serializing and de-serializing
ABI-relevant artifacts.The set of artifacts that we are interested in is made of constructions
like type, variables, functions and declarations of a given library or program.
For a given program or library,this set of constructions is called an ABI corpus.

%package devel
Summary:             Shared library and header files for libabigail
Requires:            %{name} = %{version}-%{release}

%description devel
This package contains a shared library and the associated header files to write ABI analysis tools for libabigail.

%package help
Summary:             Man pages,texinfo files and html manuals for libabigail
Provides:            libabigail-doc = %{version}-%{release}
Obsoletes:           libabigail-doc < %{version}-%{release}
Buildarch:           noarch

%description help
The package contains man pages,texinfo files and html manuals for libabigail.

%prep
%autosetup -n libabigail-%{version} -p1

%build
%configure --disable-silent-rules --disable-static
%make_build
cd doc
make html-doc
cd manuals
make html-doc
make man
make info
cd -
cd ..

%install
%make_install
%delete_la
make -C doc/manuals install-man-and-info-doc DESTDIR=%{buildroot}

%check
time make check || (cat tests/test-suite.log && exit 2)
if test $? -ne 0; then
  cat tests/tests-suite.log
fi

%files
%{_bindir}/abicompat
%{_bindir}/abidiff
%{_bindir}/abidw
%{_bindir}/abilint
%{_bindir}/abipkgdiff
%{_bindir}/kmidiff
%{_libdir}/libabigail.so.5
%{_libdir}/libabigail.so.5.*
%{_libdir}/libabigail/default.abignore

%files devel
%{_libdir}/libabigail.so
%{_libdir}/pkgconfig/libabigail.pc
%{_includedir}/*
%{_datadir}/aclocal/abigail.m4

%files help
%{_mandir}/man1/*
%{_mandir}/man7/*
%{_infodir}/abigail.info*
%doc doc/manuals/html/* README AUTHORS ChangeLog

%changelog
* Wed Jan 01 2025 Funda Wang <fundawang@yeah.net> - 2.6-1
- update to 2.6

* Thu Nov 21 2024 zhouyi <zhouyi198@h-partners.com> - 2.5-3
- Lfs url change

* Tue Nov 19 2024 zhouyi <zhouyi198@h-partners.com> - 2.5-2
- Add Lfs config
  Edit libabigail.spec

* Tue Jul 09 2024 xu_ping <707078654@qq.com> - 2.5-1
- Upgrade to 2.5
- Package libabigail.so.4 rather than the previous libabigail.so.3.

* Wed Jan 10 2024 yaoxin <yao_xin001@hoperun.com> - 2.4-1
- Upgrade to 2.4

* Sat Jul 29 2023 xu_ping <707078654@qq.com> - 2.2-3
- fix install error due to sphinx upgrade remove jquery.js

* Wed Apr 19 2023 ChenYanpan <chenyanpan@xfusion.com> - 2.2-2
- Upload the integral source archive file -- libabigail-2.2.tar.xz

* Wed Apr 12 2023 xu_ping <707078654@qq.com> - 2.2-1
- Upgrade to 2.2

* Fri Dec  9 2022 Xiaole He <hexiaole@kylinos.cn> - 2.0-4
- backport patch:0006 to fix coredump of libxul.so from thunderbird rpm

* Tue Oct 18 2022 Xiaole He <hexiaole@kylinos.cn> - 2.0-3
- backport patch:0003 to add missing else
- backport patch:0004 to optimize if construction
- backport patch:0005 to fix typo

* Wed Sep 21 2022 Xiaole He <hexiaole@kylinos.cn> - 2.0-2
- backport patch:0002 to fix problematic comment

* Sat Aug 27 2022 tianlijing <tianlijing@kylinos.cn> - 2.0-1
- update to 2.0

* Thu May 20 2021 caodongxia <caodongxia@huawei.com> - 1.6-4
- Fix compiler-flags

* Thu Jul 30 2020 Shinwell Hu <micromotive@qq.com> - 1.6-3
- Remove extra Requires.

* Mon Jun 8 2020 leiju <leiju4@huawei.com> - 1.6-2
- Package init
